import React from 'react';
import ReactDOM from 'react-dom';
import Clock from './components/clock';
import App from './components/app';
import { Provider } from 'react-redux';
import todoReducers from '../src/reducers/index';
import { createStore } from 'redux';
import { BrowserRouter as Router } from 'react-router-dom'

//console.log(`${Object.keys(Constants)}`);

ReactDOM.render(
    <Provider store={createStore(todoReducers)}>
        <Router>
            <App />
        </Router>
    </Provider>,
    document.getElementById('content')
);