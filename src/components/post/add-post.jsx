import React from 'react'

export default class AddPost extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <input ref={node => {
                    this.edtTitle = node;
                }} />

                <input ref={node => {
                    this.edtContent = node;
                }} />

                <button onClick={() => {
                    this.props.onAddPostClick(this.edtTitle.value, this.edtContent.value);

                    this.edtTitle.value = '';
                    this.edtContent.value = '';
                }}>
                    Add Post
                </button>
            </div>
        );
    }
}
