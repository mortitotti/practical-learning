import React from 'react';
import { Link } from 'react-router-dom'

export default class Post extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <li onClick={this.props.onPostRowClick} style={{
                textDecoration: this.props.isPublished ? 'line-through' : 'none'
            }}>
                {/* <Link to={`/detail/${this.props.id}`}>{this.props.title}</Link> */}
                {this.props.title}
            </li>
        )
    }
}
