import React from 'react'
import Post from './post'

export default class PostList extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ul>
                {this.props.posts.map(post => {
                    return <Post key={post.id}
                        {...post}
                        onPostRowClick={() => this.props.onPostRowClick(post.id)} />
                })}
            </ul>
        );
    }
}