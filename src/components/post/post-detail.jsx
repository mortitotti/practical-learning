import React from 'react';

export default class PostDetail extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (<div>
            <h2>{this.props.match.params.id}</h2>
        </div>);
    }
}