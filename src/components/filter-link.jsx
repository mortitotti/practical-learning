import React from 'react';
import { connect } from 'react-redux'
import { Actions } from '../constants'

export default class FilterLink extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        if (this.props.currentFilter == this.props.filter) {
            return <span>{this.props.title}</span>
        }
    
        return (
            <a href='#'
                onClick={(e) => {
                    e.preventDefault();
                    this.props.onFilterClick(this.props.filter);
                }}
            >
            {this.props.title}
            </a>
        );
    }
}
