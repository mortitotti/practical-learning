import React from 'react'
import FilterLink from './filter-link'

export default class PostFooter extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <p>
                <FilterLink title="Show All" filter="SHOW_ALL"
                    currentFilter={this.props.currentFilter}
                    onFilterClick={this.props.onFilterClick} />                    
                {' '}
                <FilterLink title="Published" filter="SHOW_PUBLIUSHED"
                    currentFilter={this.props.currentFilter}
                    onFilterClick={this.props.onFilterClick} />
                {' '}
                <FilterLink title="UnPublished" filter="SHOW_UNPUBLIUSHED"
                    currentFilter={this.props.currentFilter}
                    onFilterClick={this.props.onFilterClick} />
            </p>
        )
    }
}