import React from 'react';
import AppRoutes from '../routes';
import ActionBarItem from './action-bar-item';
import '../scss/main.scss'

export default class ActionBar extends React.Component {

    constructor(props) {
        super(props);
    }

    gotoPage(tag, e) {
        alert(tag);
    }

    render() {
        var divStyle = {
            float: 'right'
        };

        return (
            <div className="action-bar">
                <ul className="list-unstyled">
                    {
                        AppRoutes.map((value, index) => {
                            return <ActionBarItem key={index} path={value.path} label={value.label} />
                        })
                    }

                    <li style={divStyle}>
                        <a href="http://www.google.com">
                            <span>Login</span>
                        </a>
                    </li>
                </ul>
            </div>
        );
    }
}