import React from 'react'
import ActionBar from './action-bar'
import { connect } from 'react-redux'
import { Actions } from '../constants'
import PostDetail from './post/post-detail'
import FilterLink from './filter-link'
import PostFooter from './post-footer'
import '../scss/main.scss'
import Post from './post/post'
import PostList from './post/post-list'
import AddPost from './post/add-post'
import { Route, Link } from 'react-router-dom'

const getVisiblePost = (
    posts,
    filter
) => {
    switch (filter) {
        case 'SHOW_PUBLIUSHED': {
            return posts.filter(p => p.isPublished);
        }
        case 'SHOW_UNPUBLIUSHED': {
            return posts.filter(p => !p.isPublished);
        }
        default:
            return posts;
    }
}

export class Layout extends React.Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
    }

    render() {

        const visiblePosts = getVisiblePost(this.props.posts, this.props.postVisibilityFilter);

        return (
            <div>
                <div>
                    <AddPost onAddPostClick={(title, content) => {
                        this.props.dispatch({
                            type: Actions.ADD_POST,
                            payload: {
                                title: title,
                                content: content
                            }
                        });
                    }} />

                    <PostList posts={visiblePosts} onPostRowClick={(id) => {
                        this.props.dispatch({
                            type: Actions.TOGGLE_PUBLISH_POST_DATE,
                            payload: {
                                id: id
                            }
                        });
                    }} />

                    <hr />

                    <PostFooter
                        currentFilter={this.props.postVisibilityFilter}
                        onFilterClick={(filter) =>
                            this.props.dispatch({
                                type: Actions.SET_POST_VISIBLITY_FILTER,
                                payload: filter
                            })
                        } />

                    <Route path="/detail/:id" component={PostDetail} />
                </div>
            </div>
        );
    }
}

export default connect((store, ownProps) => {
    return {
        posts: store.posts,
        postVisibilityFilter: store.postVisibilityFilter
    }
})(Layout); 
