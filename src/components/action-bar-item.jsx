import React from 'react';
import { Link } from 'react-router-dom'

export default class ActionBarItem extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <li>
                <Link to={this.props.path}>{this.props.label}</Link>
            </li>
        );
    }
}