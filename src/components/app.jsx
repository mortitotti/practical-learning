import React from 'react'
import AppRoutes from '../routes'
import ActionBar from './action-bar'
import { Route, Switch } from 'react-router-dom'
import AboutUs from '../components/about-us'

export default class App extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div>
                <ActionBar />
                <Switch>
                {
                    AppRoutes.map((value, index) => {
                       return (value.exact) ? <Route exact path={value.path} key={index} component={value.component}/>
                                           : <Route path={value.path} key={index} component={value.component}/>
                    })
                }
                </Switch>
            </div>            
        )
    }
}