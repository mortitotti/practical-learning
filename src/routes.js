import Home from './components/home'
import AboutUs from './components/about-us'
import ContactUs from './components/contact-us'

const AppRoutes = [
    {
        path: '/',
        exact: true,
        label: 'Home',
        component: Home
    },
    {
        path: '/aboutus',
        label: 'About us',
        component: AboutUs
    },
    {
        path: '/contactus',
        label: 'Contact with us',
        component: ContactUs
    }
];

export default AppRoutes;