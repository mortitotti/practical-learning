import { Actions } from '../constants';
import { allPosts } from '../initialState';
import { guid } from '../services/helpers';

export const posts = (state = allPosts, action) => {
    switch (action.type) {
        case Actions.ADD_POST: {
            return [...state, post(undefined, action)]
        }
        case Actions.UPDATE_POST: {
            return state.map(p => post(p, action))
        }
        case Actions.TOGGLE_PUBLISH_POST_DATE: {
            return state.map(p => post(p, action))
        }
        default: return state;
    }
}

const post = (state, action) => {
    var payload = action.payload;

    switch (action.type) {
        case Actions.ADD_POST: {
            return {
                id: guid(),
                title: payload.title,
                content: payload.content,
                creartedOn: Date.now()
            }
        }
        case Actions.UPDATE_POST: {
            if (state.id != payload.id)
                return state;

            return Object.assign({}, state, {
                title: payload.title,
                content: payload.content,
                isPublished: payload.isPublished
            });
        }
        case Actions.TOGGLE_PUBLISH_POST_DATE: {
            if (state.id != payload.id)
                return state;

            return Object.assign({}, state, {
                isPublished: !state.isPublished
            });
        }
        default: return state;
    }
}