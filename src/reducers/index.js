import { posts } from './posts';
import { postVisibilityFilter } from './post-visibility-filter';
import { combineReducers } from 'redux';

/*
const combineReducers = (reducers) => {
    return (state = {}, action) => {
        return Object.keys(reducers).reduce((nextState, key) => {
                nextState[key] = reducers[key](state[key], action);
                return nextState;
            },
            {}
        );
    };
};
*/

export default combineReducers({ posts, postVisibilityFilter });

/*
export default (state = {}, action) => {
    return {
        todos: todos(state.todos, action),
        visibilityFilter: visibilityFilter(state.visibilityFilter, action)
    };
}
*/
