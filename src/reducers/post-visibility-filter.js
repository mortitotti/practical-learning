import { Actions } from '../constants';

export const postVisibilityFilter = (state = 'SHOW_ALL', action) => {
    var payload = action.payload;

    switch (action.type) {
        case Actions.SET_POST_VISIBLITY_FILTER: 
            return payload;
        default:
            return state;
    }
}