export const visibilityFilter = (state = 'SHOW_ALL', action) => {
    var payload = action.payload;
    switch (action.type) {
        case 'SET_VISIBILITY_FILTER':
            return payload.filter;
        default: return state;
    }
}

const todo = (state, action) => {
    var payload = action.payload;

    switch (action.type) {
        case 'ADD_TODO': {
            return {
                id: payload.id,
                text: payload.text,
                completed: false
            }
        }
        case 'TOGGLE_TODO': {
            if (state.id != payload.id)
                return state;

            return Object.assign({}, state, { completed: !state.completed });
        }

        default: return state;
    }
}

export const todos = (state = [], action) => {
    switch (action.type) {
        case 'ADD_TODO': {
            return [...state, todo(undefined, action)];
        }
        case 'TOGGLE_TODO': {
            return state.map(t => todo(t, action));
        }
        default: return state;
    }
}