var webpackConfig = require('./webpack.config.js');
webpackConfig.devtool = 'inline-source-map';

// Karma configuration
module.exports = function (config) {
    config.set({
        // base path that will be used to resolve all patterns (eg. files, exclude)
        basePath: '',
        frameworks: ['jasmine'],
        plugins: [
            'karma-chrome-launcher',
            'karma-sourcemap-loader',
            'karma-webpack',
            'karma-jasmine',
            'deep-freeze'
        ],

        // list of files / patterns to load in the browser
        files: [
            // load all global libs in here
            // ./node_modules/angular/angular.js

            // one entry point for all 
            //'./test/test_index.js'
            
            './test/*.spec.js'
        ],

        // list of files to exclude
        exclude: [
        ],

        preprocessors: {
            './test/*.spec.js': ['webpack', 'sourcemap']
            //'test/test_index.js': ['webpack', 'sourcemap'],
        },
        reporters: ['progress'],
        port: 9876,
        colors: true,
        logLevel: config.LOG_INFO,
        autoWatch: true,
        browsers: ['Chrome'],
        singleRun: false,
        concurrency: Infinity,

        // Webpack Configuration
        webpack: webpackConfig,
        webpackMiddleware: {
            // webpack-dev-middleware configuration
            stats: 'errors-only'
        }
    })
}
