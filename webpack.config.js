const path = require("path");
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

const outputPath = path.join(__dirname, '/dist');
const assetsPath = path.join(__dirname, '/assets');
const pkg = require('./package.json');
const Karma = require("karma-webpack");
const extractSass = new ExtractTextPlugin({
    filename: "[name].[contenthash].css",
    disable: process.env.NODE_ENV === "development"
});

module.exports = {
    entry: {
        [pkg.name]: ["./src/main.js"]
    },

    output: {
        path: outputPath,
        publicPath: "/",
        filename: '[name].[chunkhash].js'
    },

    resolve: {
        extensions: ['scss', '.js', '.jsx', '.json'],
        alias: {
            components: path.resolve(__dirname, './src/components')
        }
    },
    
     devServer: {
        contentBase: [outputPath, assetsPath],
        compress: true,
        port: 8000,
        historyApiFallback: true,
        proxy: {
        '/**': {
            target: '/index.html',
            secure: false,
            bypass: function (req, res, opt) {
            if (req.headers.accept.indexOf('html') !== -1) {
                return '/index.html';
            }
            return res.status(404)
                .send('Not found');
            }
        }
        }
    },

    module: {
        rules: [
            // javascript
            {
                test: /\.(jsx|js)$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['env', 'es2015', 'react']
                    }
                }
            },

            /*
            // css
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                  fallback: "style-loader",
                  use: "css-loader"
                })
            },
            */

            // scss
            {
                test: /\.scss$/,
                exclude: /(node_modules)/,
                use: extractSass.extract({
                    use: [{
                        loader: "css-loader"
                    },{
                        loader: "postcss-loader"
                    }, {
                        loader: "sass-loader"
                    }],
                    fallback: 'style-loader'
                })
            },

            // fonts
            {
                test: /\.(ttf|eot|woff|woff2)$/,
                loader: 'file-loader',
                options: {
                    name: 'fonts/[name].[ext]'
                }
            },

            // images
            {
                test: /\.(png|jpg|gif|svg)(\?.*)?$/,
                loader: 'url-loader',
                query: {
                    limit: 10000
                }
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(outputPath),
        new HtmlWebpackPlugin({
            template: './index.html',
            inject: 'body'
        }),
        extractSass/*,
        Karma
        ,
        new ExtractTextPlugin({
            filename: "[name].[contenthash].css"
        })*/        
    ]/*,
    watch: true*/
};