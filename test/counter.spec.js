const counter = (state = 0, action) => {
    switch (action.type) {
        case 'INCREMENT':
            return state + 1;
        case 'DECREMENT':
            return state - 1;
        default:
            return state;
    }
}

describe("Increase number", function () {
    it("test A", function () {
        var number = counter(0, { type: 'INCREMENT' });
        expect(number).toBe(1);
    });

    it("test B", function () {
        var number = counter(1, { type: 'INCREMENT' });
        expect(number).toBe(2);
    });

    it("test C", function () {
        var number = counter(undefined, {});
        expect(number).toBe(0);
    });
});

describe("Decrease number", function () {
    it("test A", function () {
        var number = counter(2, { type: 'DECREMENT' });
        expect(number).toBe(1);
    });

    it("test B", function () {
        var number = counter(1, { type: 'DECREMENT' });
        expect(number).toBe(0);
    });
});