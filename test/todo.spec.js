var deepFreeze = require('deep-freeze');
import todoReducers from '../src/reducers/index';
import { createStore } from 'redux';

describe("Toggle todo", function () {
    it("test A", function () {

        const stateBefore = {
            todos: [
                {
                    id: 0,
                    text: 'Learn redux',
                    completed: false
                },
                {
                    id: 1,
                    text: 'Go shopping',
                    completed: false
                }
            ], visibilityFilter: 'SHOW_ALL'
        };

        const action = {
            type: 'TOGGLE_TODO',
            id: 1
        };

        const stateAfter = {
            todos: [
                {
                    id: 0,
                    text: 'Learn redux',
                    completed: false
                },
                {
                    id: 1,
                    text: 'Go shopping',
                    completed: true
                }
            ], visibilityFilter: 'SHOW_ALL'
        };

        deepFreeze(stateBefore);
        deepFreeze(action);

        expect(todoReducers(stateBefore, action)).toEqual(stateAfter);
    });

    it("Test B", function () {

        const stateAfter = {
            todos: [{
                id: 1,
                text: 'Go To School',
                completed: true
            }], visibilityFilter: 'SHOW_ALL'
        };

        var store = createStore(todoReducers);
        store.dispatch({
            type: 'ADD_TODO',
            id: 1,
            text: 'Go To School'
        });
        store.dispatch({
            type: 'TOGGLE_TODO',
            id: 1
        });

        expect(store.getState()).toEqual(stateAfter);
    });
});

describe("Add todo", function () {
    it("test A", function () {
        const stateBefore = {};
        const action = {
            type: 'ADD_TODO',
            id: 0,
            text: 'Learn redux'
        };
        const stateAfter = {
            todos: [{
                id: 0,
                text: 'Learn redux',
                completed: false
            }]
            , visibilityFilter: 'SHOW_ALL'
        };

        deepFreeze(stateBefore);
        deepFreeze(action);

        expect(todoReducers(stateBefore, action)).toEqual(stateAfter);
    });

    it("Test B", function () {
        const stateAfter = {
            todos: [{
                id: 1,
                text: 'Go To School',
                completed: false
            }], visibilityFilter: 'SHOW_ALL'
        };

        var store = createStore(todoReducers);
        store.dispatch({
            type: 'ADD_TODO',
            id: 1,
            text: 'Go To School'
        });

        expect(store.getState()).toEqual(stateAfter);
    });
});

describe("Visibility Filter", function () {
    it("Test A", function () {

        const stateBefore = {};

        const stateAfter = {
            todos: [], visibilityFilter: 'SHOW_COMPLETED'
        };

        var store = createStore(todoReducers);
        store.dispatch({
            type: 'SET_VISIBILITY_FILTER',
            filter: 'SHOW_COMPLETED'
        });

        expect(store.getState()).toEqual(stateAfter);
    });
});
